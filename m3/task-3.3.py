import boto3,time
from boto3.dynamodb.conditions import Key

def create_table(name,dynamodb=None):
    if not dynamodb:
     dynamodb = boto3.client('dynamodb')
    try:
     table = dynamodb.create_table(
         TableName=name,
         KeySchema=[
             {
                 'AttributeName': 'year',
                 'KeyType': 'HASH'  # Partition key
             },
             {
                 'AttributeName': 'title',
                 'KeyType': 'RANGE'  # Sort key
             }
         ],
         AttributeDefinitions=[
             {
                 'AttributeName': 'year',
                 'AttributeType': 'N'
             },
             {
                 'AttributeName': 'title',
                 'AttributeType': 'S'
             }, 

         ],
         ProvisionedThroughput={
             'ReadCapacityUnits': 5,
             'WriteCapacityUnits': 5
         }
     )
     print("Waiting for table:", name,"being active")
     dynamodb.get_waiter('table_exists').wait(TableName=name)
    except dynamodb.exceptions.ResourceInUseException:
     print("Table exhist. Pass the create_table()")

def add_movie(name, title, year, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table(name)
    response = table.put_item(
       Item={
            'year': year,
            'title': title,
        }
    )
    print("Added to table:", name , "", title ,",", year)
    return response

def list_tables(dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.client('dynamodb')
    response = dynamodb.list_tables()['TableNames']
    return response

def delete_table(name, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.client('dynamodb')

    delete = dynamodb.delete_table(
                                    TableName=name
                                  )
    print(db, "Table Deleted")
    return delete

def query_movies(name, year, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table(name)
    response = table.query(
        KeyConditionExpression=Key('year').eq(year)
    )
    return response['Items']    

def scan_table(name, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(name)    
    response = table.scan(TableName=name)
    return response['Items']


if __name__ == '__main__':
    #INITIAL VARS
    yearq = 2015
    db="My_Movie"
    movie_data={"The Big New Movie": 2015,
                "Home Alone": 2005,
                "Miami police": 1990}

    #Creating Table
    movie_table = create_table(db)

    #Listing tables
    existing_tables = list_tables()
    print(existing_tables)

    #Puting new value
    for key,val in movie_data.items():
     movie_resp = add_movie(db,key,val)

    #Query for item
    print("Trying to find movie release at", yearq)
    print("------------------------------------------")
    qr = query_movies(db,yearq)
    for movie in qr:
     print(movie['year'], ":", movie['title'])
    print("------------------------------------------") 

    #Scanning table
    print("\nScaning whole table")
    print("------------------------------------------")
    scanned = scan_table(db)
    print(scanned)
    print("------------------------------------------")

    #Deleting DB
    time.sleep(2)
    delete = delete_table(db)
